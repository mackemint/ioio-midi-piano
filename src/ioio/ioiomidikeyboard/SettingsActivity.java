package ioio.ioiomidikeyboard;

import android.os.Bundle;


import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.app.Activity;
import android.content.Intent;

public class SettingsActivity extends Activity
{

	/**
	 * This contains the selected value for the spinner
	 */
	public static int yCCVal = 1;
	public static int chVal = 0;
	
	private NumberPicker ccPicker, chPicker;
	private TextView ccView, chView;

	private Button selectionButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		final String CC_STRING = getResources().getString(R.string.cc_string);
		final String CH_STRING = getResources().getString(R.string.ch_string);

		
		String[] ccVals = new String[120];
		String[] chVals = new String[16];
		
		
		ccPicker = (NumberPicker) findViewById(R.id.ccPicker);
		chPicker = (NumberPicker) findViewById(R.id.chPicker);
		ccView = (TextView) findViewById(R.id.ccView);
		chView = (TextView) findViewById(R.id.chView);
		selectionButton = (Button) findViewById(R.id.selectionButton);
		
		
		for(int i = 1; i< ccVals.length; i++)
			ccVals[i] = Integer.toString(i);
		ccPicker.setMinValue(1);
		ccPicker.setMaxValue(ccVals.length);
		ccPicker.setValue(yCCVal);
		ccPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		
		for(int i = 1;  i < chVals.length; i++)
			chVals[i] = Integer.toString(i);
		chPicker.setMinValue(1);
		chPicker.setMaxValue(chVals.length);
		// To keep a zero-index while showing the user a 1-index selection
		chPicker.setValue(chVal+=1);
		chPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
				
/*		
		ccView.setText(CC_STRING + yCCVal);
		chView.setText(CH_STRING + chVal + 1);
*/		
		selectionButton.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				yCCVal = ccPicker.getValue();
				ccView.setText(CC_STRING + yCCVal);
				
				chVal = chPicker.getValue() - 1;
				int displayValue = chVal + 1;
				chView.setText(CH_STRING + displayValue);
				
				Intent openKeyboard = new Intent(SettingsActivity.this, IOIOMIDIActivity.class);
				startActivity(openKeyboard);
			}
		});
		
		
	
	}

	
}

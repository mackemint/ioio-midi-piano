package ioio.ioiomidikeyboard;

import ioio.javax.sound.midi.InvalidMidiDataException;
import ioio.javax.sound.midi.MidiMessage;
import ioio.javax.sound.midi.ShortMessage;
import ioio.lib.api.DigitalOutput.Spec;
import ioio.lib.api.DigitalOutput.Spec.Mode;
import ioio.lib.api.Uart;
import ioio.lib.api.Uart.Parity;
import ioio.lib.api.Uart.StopBits;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOActivity;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ArrayBlockingQueue;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
//import android.widget.TextView;

/**
 * This is the main activity of the HelloIOIO example application.
 * 
 */

public class IOIOMIDIActivity extends IOIOActivity implements View.OnTouchListener
{

	private XYEvents xyEvent =  new XYEvents();
	
	/** a queue of midi bytes, originally 100**/
	private final static int OUT_QUEUE_SIZE = 200;
	private final ArrayBlockingQueue<MidiMessage> out_queue = new ArrayBlockingQueue<MidiMessage>(OUT_QUEUE_SIZE);


	private RelativeLayout keyC, keyD, keyE, keyF, keyG, keyA, keyB, keyC2;	 
	private Button keyCs, keyDs,  keyFs,keyGs, keyAs , keyCs2;
	
	//private TextView tView;
	
	private final static String DEBUG_TAG = "IOIOMIDI";


	/**
	 * This is used for the CC values in the modulation methods.
	 * yCC defaults to CC1; mod wheel
	 */
	private int yCC;

	/**
	 * Transmitted MIDI channel. Set in the Settings activity
	 */
	private int MIDIChannel;
	
	/**
	 * This is used by the transpose command
	 */
	private int transposeOctave = 0;

	/**
	 * Sensor handlers used by app
	 */
	private SensorManager sensorManager;
	private Sensor accelerometer;
	private SensorEventListener sensorListener;

	/**
	 * Called when the activity is first created. Here we normally initialize
	 * our GUI.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		// Sets orientation to both sides Landscape
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		
		
		defineButtons();
		setKeyListeners();
		registerSensors();
		
		/**
		 * Default values for MIDI events
		 */
		yCC = 1;
		MIDIChannel = 0;

		
		

	}
	
	
	@Override
	public void onResume()
	{
		super.onResume();
		SettingsActivity settings = new SettingsActivity();
		
		yCC = settings.yCCVal;
		MIDIChannel = settings.chVal;
		
		sensorManager.registerListener(sensorListener, accelerometer, 
				SensorManager.SENSOR_DELAY_FASTEST);
		
		
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		sensorManager.unregisterListener(sensorListener);
		
		ActionBar actionBar = getActionBar();
		actionBar.show();
	}
	
	/**
	 * Creates a menu for settings of transposition and CC values
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.settings_menu, menu);
		
		return true;
		
	}
	
	
	/**
	 * 
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		
		switch(item.getItemId())
		{
		
			case R.id.cc_settings:
				Intent openSettings = new Intent(IOIOMIDIActivity.this, SettingsActivity.class);
				startActivity(openSettings);
				return true;
				

			case R.id.minusone:
				transposeOctave += -12;
//				octaveView.setText("Octave is: " + transposeOctave);
				return true;
			case R.id.plusone:
				transposeOctave += 12;
//				octaveView.setText("Octave is: " + transposeOctave);
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}

	}
	@Override
	/**	
	 * This is the general onTouch method for key presses 
	 * that can be performed
	 * 
	 * @param view		what view is affected
	 * @param motion		what motion is occurring
	 * 
	 */
	public boolean onTouch(View view, MotionEvent motion)
	{
		// When you are playing, the system bar fades out
		View rootView = getWindow().getDecorView();
		rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE); 
		
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		
		int note = 60;
		int noteValue = 0;
		
		/**
		 *  Takes the tag Id from the view and converts it to an int
		 *  for the note value
		 */
		String tagId = (String) view.getTag();
		noteValue = Integer.parseInt(tagId);
		
		note += noteValue + transposeOctave;
		
		synchronized(motion)
		{
			try
			{
				motion.wait(16);
				motionTracker(motion, note);
			}
			catch (InterruptedException e)
			{
				return true;
		
			}
		}
		return false;
	}


	/**
	 * 
	 * motionTracker metod, by macke
	 * This is utilized by the implemented onTouch method to determine what actions are available 
	 * by the pressed key.
	 * 
	 * @param event	what's going on with the view
	 * @param n	note value for playNote & stopNote
	 */
	
	/**
	 *  Checks if the finger has moved over the screen and if 
	 *  there is a need to reset the pitch bend message
	 */
	private boolean moved = false;
	
	public boolean motionTracker(MotionEvent event, int n)
	{
		int note = n;	
		
		
		
		switch(event.getAction())
		{
		
		case MotionEvent.ACTION_DOWN:
				xyEvent.setInitial(event);
				playNote(note);
			break; 
			
			/**
			 * Handles modulation events
			 */
			
		case MotionEvent.ACTION_MOVE:
			int data1;
			int data2;
				
			/**
			 * The threshold value to reach before pitch bending/y-modulating
			 */
			boolean xOK = xyEvent.getXThreshold(event)[0];
			boolean yOK = xyEvent.getYThreshold(event)[0];
			
			/**
			 * These are a check to avoid flooding 
			 * with messages to the output queue
			 */
			boolean lastX = xyEvent.getLastX(event);
			boolean lastY = xyEvent.getLastY(event);
			
				if (xOK && lastX)
				{
					moved = true;
					
					int xMod = xyEvent.eventActions(event)[0];
					System.out.println("int value: " + xMod);
					
					data1 = xMod & 0x7f;
					/**TODO
					 * Might have to be (xMod >> 8)
					 */
					data2 = (xMod >> 7) & 0x7f; 
					xModulation(data1, data2);
					
				}
				
				else if(yOK && lastY)
				{
					moved = true;
					
					int yMod = xyEvent.eventActions(event)[1];	
					
					data1 = (yMod & 0x7f);
					data2 = yMod; //For bigger resolutions: (xMod >> 7) & 0x7f
					yModulation(data1, data2);
					
				}
				
				
				
			break;
			
		case MotionEvent.ACTION_UP:
				xyEvent.setLast(event);
				stopNote(note);
				
				if(moved)
					resetModulation();
				
				moved = false;	

			break;
		}
		return false;
	}
	

	/**
		 * Defines buttons (keys) used in application 
		 */
		private void defineButtons()
		{
			//tView = (TextView) findViewById(R.id.textView);
	
			keyC = (RelativeLayout) findViewById(R.id.c);
			keyD = (RelativeLayout) findViewById(R.id.d);
			keyE = (RelativeLayout) findViewById(R.id.e);
			keyF = (RelativeLayout) findViewById(R.id.f);
			keyG = (RelativeLayout) findViewById(R.id.g);
			keyA = (RelativeLayout) findViewById(R.id.a);
			keyB = (RelativeLayout) findViewById(R.id.b);
			keyC2 = (RelativeLayout) findViewById(R.id.c2);
			
			keyCs = (Button) findViewById(R.id.cs);
			keyDs = (Button) findViewById(R.id.ds);
			keyFs = (Button) findViewById(R.id.fs);
			keyGs = (Button) findViewById(R.id.gs);
			keyAs = (Button) findViewById(R.id.as);
			keyCs2 = (Button) findViewById(R.id.cs2);
			
	}


	/**
	 * General touch listener method for all keys in app
	 */
	private void setKeyListeners()
	{
		keyC.setOnTouchListener(this);
		keyCs.setOnTouchListener(this);
		keyD.setOnTouchListener(this);
		keyDs.setOnTouchListener(this);
		keyE.setOnTouchListener(this);
		keyF.setOnTouchListener(this);
		keyFs.setOnTouchListener(this);
		keyG.setOnTouchListener(this);
		keyGs.setOnTouchListener(this);
		keyA.setOnTouchListener(this);
		keyAs.setOnTouchListener(this);
		keyB.setOnTouchListener(this);
		keyC2.setOnTouchListener(this);
		keyCs2.setOnTouchListener(this);
		
	}

	/**
	 * Sensor register method
	 */
	private void registerSensors()
	{
		
		sensorListener = new MySensorEventListener();
		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	}


	/**
	 * General method used by all motion events to offer a message 
	 * to the BlockingQueue.
	 * 
	 * @param st 	the status byte MSB (command)
	 * @param dataByte1	the first data byte
	 * @param dataByte2 the second data byte
	 * 
	 */
	private void accessShortmessage(int st, int dataByte1, int dataByte2)
	{
		int ch = this.MIDIChannel;
		ShortMessage msg = new ShortMessage();
		
		try 
		{
			msg.setMessage(st, ch, dataByte1, dataByte2);
			out_queue.offer(msg);
		}
	
		catch (InvalidMidiDataException e)
		{
			Log.e(DEBUG_TAG,"InvalidMidiDataException caught");
	
		}
	
	
	}

	/**
	 * Used by motionTracker method when the finger moves over the screen
	 * on the x-axis.
	 * 
	 * Uses two variables that represents data byte 1 & 2 in the ShortMessage:
	 * @param d1		the MSB or CC#
	 * @param d2		the LSB (for aftertouch or pitchbend) or control value MSB 
	 * 
	 */
	
	protected void xModulation(int d1, int d2)
	{
		int command = ShortMessage.PITCH_BEND;
		int dataByte1 = d1;
		int dataByte2 = d2;
		accessShortmessage(command, dataByte1, dataByte2); //dataByte1 needs to be LSB and dataByte2 MSB ....
		int wholeByte = ((dataByte2<<8) | (dataByte1 & 0xFF));
		Log.i(DEBUG_TAG,"whole byte: " + wholeByte);
		
	}

	/**
	 * Used by motionTracker method when the finger moves over the screen on the
	 * y-axis
	 * 
	 * @param d1		the MSB or CC#
	 * @param d2		the LSB (for aftertouch or pitchbend etc) or control value MSB
	 * 
	 */
	
	
	protected void yModulation(int d1, int d2)
	{
		int command = ShortMessage.CONTROL_CHANGE;

		/*
		* This defaults to CC#1; Mod Wheel 
		*/
		int dataByte1 = yCC;		

		int dataByte2 = d2;
		accessShortmessage(command, dataByte1, dataByte2);
		Log.i(DEBUG_TAG,"mod wheel" + dataByte2);
		
		
	}

	/**
	 * This utilizes getMessage to send a NOTE_ON message
	 * 
	 * @param n 		the note value
	 */
	protected void playNote(int n)
	{
		int command = ShortMessage.NOTE_ON;
		int noteValue = n;
		int velocity = ((MySensorEventListener) sensorListener).getVelocity();
		System.out.println("Note velocity: " + velocity);
		accessShortmessage(command, noteValue, velocity);
		Log.i(DEBUG_TAG,"playing note " + noteValue);

	}
	
	/**
	 * Utilizes getMessage to send a note on message with the velocity 0
	 *  
	 * @param n		the note value 
	 */
	protected void stopNote(int n)
	{
		/**
		 *  Not using NOTE_OFF saves 31ms of processing time.
		 *  Google MIDI Running Status
		 */
		int command = ShortMessage.NOTE_ON; 
		int velocity = 0;

		int noteValue = n;
		accessShortmessage(command, noteValue, velocity);
		Log.i(DEBUG_TAG,"stopping note " + noteValue);

	}

	/**TODO has had some problems with double size formatting
	 * 
	 * Resets the pitch bend wheel after let go.
	 *  
	 */
	private void resetModulation()
	{
		int command = ShortMessage.PITCH_BEND;
		int pitchBendZero = 8192;//16384;
		int dataByte1 = pitchBendZero & 0x7f;
		int dataByte2 = ((pitchBendZero >> 7) & 0x7f); //8
		accessShortmessage(command, dataByte1, dataByte2); //dataByte1 needs to be LSB and dataByte2 MSB ....
		Log.i(DEBUG_TAG,"reset pitch bend" + dataByte2);
		
	}

	/**
	 * This is the thread on which all the IOIO activity happens. It will be run
	 * every time the application is resumed and aborted when it is paused. The
	 * method setup() will be called right after a connection with the IOIO has
	 * been established (which might happen several times!). Then, loop() will
	 * be called repetitively until the IOIO gets disconnected.
	 * 
	 * This looper has been edited to avoid deprecations as of september 19th 2012
	 */
	class Looper extends BaseIOIOLooper
	{

		private Uart midi_out_;
		/** 
		 * The output stream used to send the midi bytes 
		 **/
		private OutputStream out;
		
		
		/**
		 * Called every time a connection with IOIO has been established.
		 * Typically used to open pins.
		 * 
		 * @throws ConnectionLostException
		 *             When IOIO connection is lost.
		 * 
		 * @see ioio.lib.util.AbstractIOIOActivity.IOIOThread#setup()
		 */
		
		
		@Override
		protected void setup() throws ConnectionLostException 
		{
			midi_out_ = ioio_.openUart(null,new Spec(7,Mode.OPEN_DRAIN), 31250,Parity.NONE,StopBits.ONE);
			out = midi_out_.getOutputStream();
		}

		/**
		 * Called repetitively while the IOIO is connected.
		 * 
		 * @throws ConnectionLostException
		 *             When IOIO connection is lost.
		 * 
		 * @see ioio.lib.util.AbstractIOIOActivity.IOIOThread#loop()
		 */
		
		@Override
		public void loop() throws ConnectionLostException 
		{	
			if (!out_queue.isEmpty())
			{
				try 
				{
					out.write(out_queue.poll().getMessage());
				} 
				catch (IOException e) 
				{
					Log.e(DEBUG_TAG,"IOIOException caught");
				}	
			}	
		}
	}

	/**
	 * A method to create our IOIO thread.
	 * 
	 * @see ioio.lib.util.AbstractIOIOActivity#createIOIOThread()
	 * 
	 * This looper has been edited to avoid deprecations as of september 19th 2012
	 */
	@Override
	protected IOIOLooper createIOIOLooper() 
	{
		return new Looper();
	}
}
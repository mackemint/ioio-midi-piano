# README for IOIO MIDI Keyboard

 * Created by Markus Kaczmarek September 2012.
 * This is part of my project for Ljudsättare I, fall semester.
 * My goal was to make a monophonic keyboard for any synthesizer that will connect wire bound through a IOIO.
 * The purpose of this project is to gain some understanding of the process of communicating via MIDI between two different units and explore the
 * possibilities of doing so in Android.
 * 
 * The controller has velocity sensitivity to some extent and uses the users gestures through motion events to
 * manipulate CC messages and pitch bend. A menu for accessing CC messages is provided as well as the possibility to change MIDI channel.
 *  
 * I wish to make a controller that is simple, intuitive and fun to use and I hope that someone will find this interesting and maybe even useful.
 * A demonstration video link for Youtube will be posted.

